<?php
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once "../vendor/autoload.php";

require_once "db.php";

$token='5542039796:AAGZzUps4dban2kiwscAjrlb3jRC7foix8M';
$bot = new \TelegramBot\Api\Client($token);
$botapi = new \TelegramBot\Api\BotApi($token);

//Код
// define encoding
header('Content-type: text/html; charset=utf-8');
// Create a bot object
$bot = new Bot();
// Processing incoming data
$bot->init('php://input');



/**
 * Class Bot
 */
class Bot
{
    // <bot_token> - generated token for our bot from @BotFather
    private $botToken = "5542039796:AAGZzUps4dban2kiwscAjrlb3jRC7foix8M";
    // address for requests to the Telegram API
    private $apiUrl = "https://api.telegram.org/bot";

    public function init($data)
    {
        // create an array from the data received from the Telegram API
        $arrData = $this->getData($data);

        // log
        // $this->setFileLog($arrData);

        if (array_key_exists('message', $arrData)) {
            $chat_id = $arrData['message']['chat']['id'];
            $message = $arrData['message']['text'];

        } elseif (array_key_exists('callback_query', $arrData)) {
            $chat_id = $arrData['callback_query']['message']['chat']['id'];
            $message = $arrData['callback_query']['data'];
        }

        $user_phone = $arrData["message"]["contact"]["phone_number"];

        $justKeyboard = $this->getKeyBoard([[["text" => "Відправити номер телефону",'request_contact' => true]]]);

        $inlineKeyboard = $this->getInlineKeyBoard([[
            ['text' => 'кнопка 1', 'callback_data' => 'button_1'],
            ['text' => 'Google', 'url' => 'https://www.google.com']
        ]]);
        switch ($message) {
            case '/start':
                $dataSend = array(
                    'text' => "ДОБРОГО ДНЯ, це бот АПРУВЕРА для верифікації. Для того, щоб отримувати код сповіщення від апрувер передайте свої контакти",
                    'chat_id' => $chat_id,
                    'reply_markup' => $justKeyboard,
                );
                $this->requestToTelegram($dataSend, "sendMessage");
                break;
            case '/buttons':
                $dataSend = array(
                    'text' => "Можете вибрати кнопки:",
                    'chat_id' => $chat_id,
                    'reply_markup' => $inlineKeyboard,
                );
                $this->requestToTelegram($dataSend, "sendMessage");
                break;
            case (preg_match('/^button_1/', $message) ? true : false):
                $dataSend = array(
                    'text' => "Дякую за підписку, бот Вас буде сповіщати",
                    'chat_id' => $chat_id,
                    'phone_number' => $user_phone
                );
                $this->getPhoneNumber($chat_id, $user_phone);
                $this->requestToTelegram($dataSend, "sendMessage");
                break;
            default:
                $dataSend = array(
                    'text' => "",
                    'chat_id' => $chat_id,
                );
                $this->requestToTelegram($dataSend, "sendMessage");
                break;
        }
    }

    /**
     * @return mysqli|void
     */
    private function getDB()
    {
        $servername = "localhost";
        $username = "appruverbot";
        $password = "VM1FnFanxUi8hXPG";
        $database = "appruverbot";

        $link = new mysqli($servername, $username, $password, $database);

        if (!$link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
        return $link;
    }

    /**
     * @param $chat_id
     * @param $user_phone
     * @return bool|mysqli_result
     */
    public function getPhoneNumber($chat_id, $user_phone)
    {
        /**
         * here is code this function
         */
        $link = $this->getDB();

        $phone = str_replace("+", "", "$user_phone");
        $revphone_number = strrev($phone);
        $sql = "INSERT INTO `users`(`user_id`, `phone_number`, `revphone_number`) VALUES ('$chat_id', '$phone', '$revphone_number')";
        $result = $link->query($sql);
        return $result;
    }

    /**
     * create an inline keyboard
     * @return string
     */
    private function getInlineKeyBoard($data)
    {
        $inlineKeyboard = array(
            "inline_keyboard" => $data,
        );
        return json_encode($inlineKeyboard);
    }

    /**
     * create a keyboard
     * @return string
     */
    private function getKeyBoard($data)
    {
        $keyboard = array(
            "keyboard" => $data,
            "one_time_keyboard" => false,
            "resize_keyboard" => true
        );
        return json_encode($keyboard);
    }
    private function setFileLog($data)
    {
        $fh = fopen('log.txt', 'a') or die('can\'t open file');
        ((is_array($data)) || (is_object($data))) ? fwrite($fh, print_r($data, TRUE) . "\n") : fwrite($fh, $data . "\n");
        fclose($fh);
    }

    /**
     * Parse what comes and convert it to an array
     * @param $data
     * @return mixed
     */
    private function getData($data)
    {
        return json_decode(file_get_contents($data), TRUE);
    }

    /** Sending a request to Telegram
     * @param $data
     * @param string $type
     * @return mixed
     */
    public function requestToTelegram($data, $type)
    {
        $result = null;

        if (is_array($data)) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $this->apiUrl . $this->botToken . '/' . $type);
            curl_setopt($ch, CURLOPT_POST, count($data));
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            $result = curl_exec($ch);
            curl_close($ch);
        }
        return $result;
    }
}



